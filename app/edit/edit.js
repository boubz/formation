'use strict';

angular.module('app')
  .config([
    '$routeProvider',
    function($routeProvider) {
      $routeProvider.when('/edit/:taskId', {
        templateUrl: 'edit/edit.html'
      });
      $routeProvider.when('/new', {
        templateUrl: 'edit/edit.html',
        newTask: true
      });
    }
  ])
  .controller('EditController', [
    '$scope',
    '$route',
    '$routeParams',
    '$location',
    // TODO injecter le filtre plannedDateAsDate
    'Task',
    function($scope, $route, $routeParams, $location, Task) {

      if ($route.current.newTask) {
        this.task = new Task($routeParams);
        this.task.$resolved = true;
        $scope.$watch(function() {
          return this.task.id;
        }.bind(this), function(id) {
          if (id) {
            $location.path('/tasks/' + id);
          }
        });
      }
      else {
        this.task = Task.get({taskId: $routeParams.taskId});
        this.task.$promise
          // TODO appliquer le filtre plannedDateAsDate
          .then(function() {
            $scope.$watch(function() {
              return this.task.id;
            }.bind(this), function(id) {
              if (!id) {
                $location.path('/');
              }
            });
          }.bind(this), function() {
            $location.path('/404');
          })
        ;
      }

      this.save = function() {
        this.error = null;
        this.task.$save()
          // TODO appliquer le filtre plannedDateAsDate
          .then(function(task) {
            $location.path('/tasks/' + task.id);
          }, function() {
            this.error = 'An error occurred on the server. Please try again.';
          }.bind(this))
        ;
      };
    }
  ])
;
